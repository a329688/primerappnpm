const sumar = require("../index");
const assert = require("assert");
//Asserts = Afirmacion, frase logica que permiter hacer casos de preba
describe("Probar la suma de dos numeros",()=>{
  //Afirmamos que cinco mas siete es 12
  it("Cinco mas siete son 12", ()=>{
    assert.equal(12, sumar(5,7));
  });
  //Afirmamos que cinci mas siete no son 57
  it("Cinco mas siete no son 57", ()=>{
    assert.notEqual("57",sumar(5,7));
  });
});//recibe afirmacion y funcion anonima
